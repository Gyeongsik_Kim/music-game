﻿using UnityEngine;
using System.Collections;

public class TimeHandler : MonoBehaviour {
    public static int time;
    private int beforeTime;
    private GameObject night, morning;
	void Start () {
        morning = GameObject.Find("morning");
        night = GameObject.Find("night");
	}
	
	// Update is called once per frame
	void Update () {
        if (beforeTime != time)
        {
            if (time < 5 || time > 20)
            {
                set(night, true);
                set(morning, false);
            }
            else
            {
                set(night, false);
                set(morning, true);
            }
            beforeTime = time;
        }
	}

    private void set(GameObject go, bool state){
        print(go.name + " : " + (state ? "활성화" : "비활성화"));
        go.SetActive(state);
    }
}
