﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

public class LoadSongInfo : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

[Serializable]
public class SongInfo
{
    public string songname;
    public string artist;
    public string songpath;
    public string albumpath;
    public int songnum;
    public int level;
    public int BPM;
    public int length;
    public int criteriabeat;
    //public 

    //JSON String을 받아서 class 안에 있는 변수의 형태로 변환해줌.
    public static SongInfo CreateFromJSON(string jsonString)
    {
        return JsonUtility.FromJson<SongInfo>(jsonString);
    }

    //해당 클래스 안에 저장되어 있는 변수를 json 형태의 string으로 변환해서 저장.
    public string SaveToString()
    {
        return JsonUtility.ToJson(this);
    }
}