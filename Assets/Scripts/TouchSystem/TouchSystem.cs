﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;

public class TouchSystem : MonoBehaviour {
    private EventSystem es;
    private string before;
    private Scene scene;
    private int stack;
	// Use this for initialization
	void Start () {
        es = EventSystem.current;
        scene = SceneManager.GetActiveScene();
        before = "";
        stack = 0;
	}

    // Update is called once per frame
    void Update() {
        GameObject select = es.currentSelectedGameObject;
        if (select != null && !select.name.Equals(before)){
            //print("Scene : " + scene.name);
            //print("Select : " + select.name);
            before = select.name;
            selectAction(scene.name, select);
        }
        if (Input.GetMouseButtonUp(0))
            before = "";
	}

    private void selectAction(string sceneName, GameObject go)
    {
        print(sceneName);
        switch (sceneName)
        {
            case "MainStart":
                mainScene(go);
                break;
            case "PauseSetting":
                pauseSettingScene(go);
                break;
            case "PrintText":
                printTextScene(go);
                break;
            case "NoteFall":
                noteFallScene(go);
                break;
            case "Touch":
                touchScene(go);
                break;
            default:
                print(sceneName);
                print(go.name); 
                break;
        }
    }

    private void noteFallScene(GameObject go)
    {
        print(go.name);

    }
    private void printTextScene(GameObject go)
    {

        print(go.name);
    }

    private void pauseSettingScene(GameObject go)
    {
        print(go.name);

    }
    private void mainScene(GameObject go)
    {
        switch (go.name)
        {
            case "StartButton":
                {
                    print("GAME START!");
                    break;
                }
            case "LoadButton":
                print("DATA LOAD");
                break;
           default:
                print(go.name);
                break;
        }
    } 

    private void touchScene(GameObject go) {
        manActionHandler.nowState = manActionHandler.State.NORMAL;
        switch (go.name)
        {
            case "blueBook":
                TimeHandler.time = 1;
                break;
            case "redBook":
                TimeHandler.time = 13;
                break;
            case "Table":
                manActionHandler.nowState = manActionHandler.State.SIT;
                break;
            default:
                break;
        }
       
    }
}
