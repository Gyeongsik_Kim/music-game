﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PrintText : MonoBehaviour {
    public float OrderPrintSec = 0.05f;
    public Text ChatString;
    private string PrintStringBuffer = "";

    public TextAsset ChatData;
    private string[] ChatDatas;
    private int i = 0;

    public bool isPrintingText = false;

    void Awake()
    {
        ChatString.text = "";
        SetChatData(ChatData);
    }

    public void SkipButton()
    {
        SkipReset();
        PrintString(ChatDatas[i]);
    }
    public void SkipReset()
    {
        StopCoroutine("OrderStringPrint");
        ResetChatString();
    }

    public void PrintString(string str)
    {
        if(isPrintingText == false)
        {
            PrintStringBuffer = str;
            if (i < ChatDatas.Length)
            {
                i++;
            }
            StartCoroutine("OrderStringPrint");
            isPrintingText = true;
        }
        else
        {
            Debug.LogError("이미 출력중이란다^^ 인생은 실전이야");
        }
    }
    private IEnumerator OrderStringPrint()
    {
        ResetChatString();

        string Words = PrintStringBuffer;
        foreach (char word in Words)
        {
            ChatString.text += word;
            yield return new WaitForSeconds(OrderPrintSec);
        }
    }

    public void ResetChatString()
    {
        ChatString.text = "";
    }

    private void SetChatData(TextAsset data)
    {
        string buffer = data.text;

        ChatDatas = buffer.Split('\n');
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.KeypadEnter))
        {
            if(isPrintingText == false)
            {
                PrintString(ChatDatas[0]);
            }
        }
    }
}
