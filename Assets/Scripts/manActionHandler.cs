﻿using UnityEngine;
using System.Collections;

public class manActionHandler : MonoBehaviour {
    public enum State { NORMAL, NORMAL_TO_SIT, SIT };
    public static State nowState;
    private GameObject normal, sit;
    void Start()
    {
        normal = GameObject.Find("Normal");
        sit = GameObject.Find("Sit");
        nowState = State.NORMAL;
    }

    // Update is called once per frame
    void Update () {
        switch (nowState)
        {
            case State.NORMAL:
                normal.SetActive(true);
                sit.SetActive(false);
                break;
            case State.NORMAL_TO_SIT:
                break;
            case State.SIT:
                normal.SetActive(false);
                sit.SetActive(true);
                break;
        }	       
	}
}
